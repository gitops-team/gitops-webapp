FROM scratch

LABEL maintaner="Walid Hadj Taieb <hadjtaieb.walid@gmail.com>"

COPY . .

EXPOSE 8085

CMD ["./main"]
